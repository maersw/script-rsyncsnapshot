
- [Overview](#overview)
- [Usage](#usage)
- [Configuration](#configuration)
  - [1. Common configuration](#1-common-configuration)
    - [RSNAP_doLocalBackup](#rsnap_dolocalbackup)
    - [RSNAP_backupRootDir](#rsnap_backuprootdir)
    - [RSNAP_commonExtraOptions](#rsnap_commonextraoptions)
    - [RSNAP_backupExtraOptions](#rsnap_backupextraoptions)
  - [2. Settings for snapshots created in local file system](#2-settings-for-snapshots-created-in-local-file-system)
    - [RSNAP_localBackupDir](#rsnap_localbackupdir)
  - [3. Settings for snapshots created on a remote machine](#3-settings-for-snapshots-created-on-a-remote-machine)
    - [RSNAP_remoteHost](#rsnap_remotehost)
    - [RSNAP_sshPort](#rsnap_sshport)
    - [RSNAP_username](#rsnap_username)
    - [RSNAP_rsyncModule](#rsnap_rsyncmodule)
    - [RSNAP_folderInModule](#rsnap_folderinmodule)
- [Setting up remote host](#setting-up-remote-host)
  - [Prerequisits:](#prerequisits)
  - [Create rsyncd.conf](#create-rsyncdconf)


# Overview
It's a pain to get [rsync](https://rsync.samba.org/) doing
what it should. 

The scripts located in this repository 
tries to simplify the process of creating 
snapshots of directories using [rsync](https://rsync.samba.org/).

The snapshots can either be stored in
local file system (for example on a USB drive) or on a remote server which is
accessable via SSH.

rsnap.sh executes rsync twice:

1. transfer data to a working directory on remote side (called aa_latest)
2. transfer data again to a remote side, the destination directory is using current date+time as name and uses hard links to aa_latest (so transfer is pretty fast)



# Usage

    ./rsnap.sh                  # use default configuration stored inside script
                                # (usually this does not make sense)
    ./rsnap.sh myConfigFile.sh  # configuration loaded from a config file

    
# Configuration

The configuration file is actually a bash script
which gets "source"d by rsnap.sh. 
In the configuration file some bash variables must be set.

The configuration section is split into 3 sections:
1. common configuration
2. configuration for snapshots stored in local file system
3. configuration stored on a remote machine

## 1. Common configuration

### RSNAP_doLocalBackup
- Set to 0 to create snapshots on a remote machine using
  a SSH connection
- Set to a value != 0 to create snapshots in local file system

### RSNAP_backupRootDir

A snapshot will be created from this local
directory. It's recommended to specify the root directory
and defining --include rules to include/exclude files or
folders from the backup.

### RSNAP_commonExtraOptions

This options will be passed to every rsync call.

This option is useful for example when there's a
need to specify for example --rsync-path)


### RSNAP_backupExtraOptions

This options will be passed to the backup creation 
call only. Useful for example to specify --include 
rules

## 2. Settings for snapshots created in local file system

### RSNAP_localBackupDir

Snapshots will be stored in this directory. 


## 3. Settings for snapshots created on a remote machine

### RSNAP_remoteHost
    
Server where to store backups

### RSNAP_sshPort
Should be self explaining

### RSNAP_username
The ssh username for performing an ssh connection.
Note: there's no password variable
existing. To get rid of password prompts setup 
public key authentication with SSH.

### RSNAP_rsyncModule
rsync module where backup will be created. Modules
can be created in rsyncd.conf. See below how to
configure the remote host

### RSNAP_folderInModule
Folder inside RSNAP_rsyncModule where snapshots 
will be stored. Please note that this folder 
**must be existing** on the server, otherwise there
will be errors.
If left empty the snapshots will be stored in the 
module directly.


# Setting up remote host
## Prerequisits:
- destination host must be reachable via SSH
- rsync must be installed on remote host
- the user used for the SSH connection must be able to run rsync as root (i. e. use root for login or allow the user to run `sudo rsync` without asking for passwords)  
  The reason why rsync must be run as root is that rsnap.sh is using the --archive option of rsync which stores file permissions, ownership an others which can be set by teh receiver only when run as root.
  

## Create rsyncd.conf
Create module entry in `/etc/rsyncd.conf`:
```
[MODULE_NAME]
lock file = /var/lock/rsyncd-MODULE_NAME
# run rsync as root for this module (uid=gid=0)!
uid = 0
gid = 0
path = /path/where/to/store/stuff
list = yes
read only = no
write only = no
use chroot = no

```

