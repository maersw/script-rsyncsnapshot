# Important notes
## Rsync/Windows
When running rsync on Windows using cygwin make sure that
/etc/fstab is using the "noacl" option like this:

	none /cygdrive cygdrive user,noacl,posix=0 0 0

Without this setting strange permission errors will be triggered
when rsync is creating directories.

See also:

- https://stackoverflow.com/questions/5798807/rsync-on-windows-wrong-permissions-for-created-directories
- https://www.itefix.net/content/permissions-filesdirectories-are-clutteredmixed

# Create a new user $rsUser
- set login shell to `bash`
- add to group `ssh`

# Create a shared folder $rsFolder

open "ACL" settings and assign

- Owner: root, Read/Write/Execute
- Group: users, None
- Others: None
- $rsUser: Read/Write (Checkbox)

# Login as root and create a home folder for $rsUser
- chown $rsUser:users /home/$rsUser
- chmod 0700 rsUser /home/$rsUser

# Create a file /home/$rsUser/rsyncd.conf
With this content:

	[$rsFolder]
	path = /srv/dev-disk-by-label-Data/$rsFolder
	list = yes
	read only = no
	write only = no
	use chroot = no
	lock file = /var/lock/rsyncd-$rsFolder

# Create backups

## Create snapshots

Create initial backup:

	rsync 					\
	--progress 				\
	--verbose 				\
	--recursive 			\
	--archive 				\
	-e ssh 					\
	/path/to/local/folder/*	\
	$rsUser@host::$rsdest/Back0
	
Create next snapshot, let rsync create hard links to first one
for unchanged files. Note the option **--link-dest** which 
points to the previous backup

	rsync 					\
	--progress 				\
	--verbose 				\
	--recursive 			\
	--archive 				\
	-e ssh 					\
	--link-dest=../Back0/ 	\
	/path/to/local/folder/*	\
	$rsUser@host::$rsdest/Back1


