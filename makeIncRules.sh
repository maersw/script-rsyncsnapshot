#! /bin/bash

printUsage()
{
read -d '' help <<- EOF
Generates "--include" rules for rsync.

Usage:
    $0 [dir1] [dir2] ... [dirN]

When using --include or --include-from rsync options
it's a pain to setup the configuration because it's not
sufficient to just pass a path like /foo/bar/baz", instead
each directory must be passed separately like:
    + /
    + /foo
    + /foo/bar
    + /foo/bar/baz
    + /foo/bar/baz/**
    - *
This script generates the configuration for directories
passed as command line arguments.

Note: This script generates absolute paths. This means
the root directory "/" must be passed as local source to rsync:
    rsync --archive --include-from=inc.txt -rsh=ssh / user@desitination::module
where "inc.txt" has been generated by this script ($0).
EOF

echo "$help"
}

makeIncludeFilter()
{
    echo "+ ""$1" 
}

getParents()
{
    local d="$1"
    
    makeIncludeFilter "$d"
    
    while [ "$d" != "/" ]
    do    
        d=$(dirname $d)
        makeIncludeFilter "$d"
    done
}

makeIncRules()
{
    for d in "$@"
    do
        if [ -d "$d" ]
        then
            d=$(realpath "$d")
            getParents "$d"
            makeIncludeFilter "$d/**"
        else
            echo "not a directory: $d" >&2
        fi
    done
}

case $1 in
    "--help"|"-h")
        printUsage
        exit 0
    ;;
esac


makeIncRules "$@" | sort -u
echo "- *"
