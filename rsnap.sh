#! /bin/bash

#########################################################################
# Copyright (C) 2017 Martin Ertl
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#########################################################################

set -e

VERSION="0.0.0"

printUsage()
{
    read -d '' help <<- EOF
Create snapshots of directories using rsync.
Usage:
    $0 [optional: configFile]
    
The configuration is done inside $0, but settings
can be overridden in the optional [configFile] which
will be "sourced" so variables can be overridden.

VERSION: $VERSION
EOF

    echo "$help"
}

case $1 in
    "--help"|"-h")
        printUsage
        exit 0
    ;;
esac





############################
# internal settings
############################

# load configFile which might override settings from above
if [[ -f $1 ]]
then
    source $1;

else
    # use settings from environment
    echo using settings from einvironment:
    echo RSNAP_doLocalBackup=$RSNAP_doLocalBackup
    echo RSNAP_backupRootDir=$RSNAP_backupRootDir
    echo RSNAP_commonExtraOptions=$RSNAP_commonExtraOptions
    echo RSNAP_backupExtraOptions=$RSNAP_backupExtraOptions
    echo RSNAP_localBackupDir=$RSNAP_localBackupDir
    echo RSNAP_remoteHost=$RSNAP_remoteHost
    echo RSNAP_sshPort=$RSNAP_sshPort
    echo RSNAP_username=$RSNAP_username
    echo RSNAP_rsyncModule=$RSNAP_rsyncModule
    echo RSNAP_folderInModule=$RSNAP_folderInModule

fi

# RSNAP_backupExtraOptions excludes everything by default
# to prevent unexpected copying of whole root fs
# (the local source is set to "/" below)
if [[ "$RSNAP_backupExtraOptions" == "--include='- *'" ]]
then
    echo "#################### WARNING ####################"
    echo "# \$RSNAP_backupExtraOptions excludes everyting"
    echo "# Think about overriding this options in a config file!"
    echo "##################################################"
fi

# A directory is created for each snapshot using this
# pattern (regular expression)
# Please note that this MUST correspond to "currentTimestamp"
# created below
snapshotPattern='[0-9]{4}-[0-9]{2}-[0-9]{2}---[0-9]{2}_[0-9]{2}_[0-9]{2}'

############################
# implementation
############################

commonRsync="rsync $RSNAP_commonExtraOptions "

destination=""

if [[ $RSNAP_doLocalBackup -ne 0 ]]
then
    # Create local backup

    # Ensure trailing slash (remove it first if existing, then append one)
    # The trailing slash is required to get the content of that directory using 
    # rsync. Without the slash the RSNAP_localBackupDir itself is returned, not its
    # content.
    destination="${RSNAP_localBackupDir%/}"
    destination="${destination}/"

else
    # Create remote backup
    
    destination="$RSNAP_username@$RSNAP_remoteHost::$RSNAP_rsyncModule/$RSNAP_folderInModule/"
    
    commonRsync="$commonRsync --rsh=\"ssh -p $RSNAP_sshPort\"";
    
    # --chmod=Du=rwx: without this option directories
    # might be created which do not have permission
    #commonRsync="$commonRsync --chmod=Du=rwx";
    
fi

latestName="aa_latest"

commonRsync="${commonRsync} --partial";
commonRsync="${commonRsync} --progress"
#commonRsync="${commonRsync} --recursive"
#commonRsync="${commonRsync} --links"
#commonRsync="${commonRsync} --devices"
#commonRsync="${commonRsync} --specials"
#commonRsync="${commonRsync} --delete"
#commonRsync="${commonRsync} --perms"
commonRsync="${commonRsync} --archive"
#commonRsync="${commonRsync} --rsync-path='sudo rsync'"


# create latest
cmd="${commonRsync} "
cmd="${cmd} $RSNAP_backupExtraOptions"
cmd="${cmd} $RSNAP_backupRootDir"
cmd="${cmd} ${destination}${latestName}"
echo "start: $cmd"
eval $cmd
echo "finished: $cmd"
echo "=========================================================="

# create snapshot, create hard links to latest
currentTimestamp=$(date +'%Y-%m-%d---%H_%M_%S')
cmd="${commonRsync} "
cmd="${cmd} --link-dest=../$latestName"
cmd="${cmd} $RSNAP_backupExtraOptions"
cmd="${cmd} $RSNAP_backupRootDir"
cmd="${cmd} ${destination}${currentTimestamp}"
echo "start: $cmd"
eval $cmd
echo "finished: $cmd"






