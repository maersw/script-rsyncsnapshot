############################
# common settings
############################

# set to =!0 to create snapshots in a local directory
# set to 0 to create snapshot on a remote machine
RSNAP_doLocalBackup=0

# A snapshot will be created from this local
# directory. It's recommended to specify the root directory
# and defining --include rules to include/exclude files or
# folders from the backup.
RSNAP_backupRootDir="/"

# This options will be passed to every rsync call.
# Be aware, this might break detection of last created snapshot!
# Useful for example when there's a need to specify for
# example --rsync-path)
RSNAP_commonExtraOptions="--verbose"
#RSNAP_commonExtraOptions="${commonRRSNAP_commonExtraOptionssync} --rsync-path='sudo rsync'";

# This options will be passed to the backup creation call
# only. Useful for example to specify --include rules
RSNAP_backupExtraOptions="--include-from=inc.txt"


############################
# local backup settings (apply when $doLocalBackup != 0)
############################

# snapshots will end up in this directory
RSNAP_localBackupDir="/cygdrive/c/mmm/rsyncBuero/script-rsyncsnapshot/backDir"

############################
# remote backup settings (apply when $doLocalBackup == 0)
############################

# Server where to store backups
RSNAP_remoteHost=10.100.200.20

# SSH Port
RSNAP_sshPort=22

# Username (assuming same username for ssh and rsync)
RSNAP_username=root

# rsync module where backup will be created
RSNAP_rsyncModule=rsModule

# folder inside rsyncModule where snapshots will be stored
# Please note that this folder must be existing on the
# server, otherwise there will be errors.
# If left empty the snapshots will be stored in the module
# directly.
RSNAP_folderInModule=foo
